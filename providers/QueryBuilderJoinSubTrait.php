<?php

namespace Empu\DbAdditive\Providers;

use Closure;
use InvalidArgumentException;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

/**
 * Extend query builder trait
 * porting methods that are specific from laravel 5.6
 * may be able to remove this if this app gets upgraded
 */
trait QueryBuilderJoinSubTrait
{
    protected function bootQueryBuilderJoinSubTrait()
    {
        QueryBuilder::macro('joinSub', function ($query, $as, $first, $operator = null, $second = null, $type = 'inner', $where = false) {
            list($query, $bindings) = $this->createSub($query);
            $expression = '('.$query.') as '.$this->grammar->wrap($as);
            $this->addBinding($bindings, 'join');
            return $this->join(new Expression($expression), $first, $operator, $second, $type, $where);
        });

        QueryBuilder::macro('leftJoinSub', function ($query, $as, $first, $operator = null, $second = null) {
            return $this->joinSub($query, $as, $first, $operator, $second, 'left');
        });

        QueryBuilder::macro('createSub', function ($query) {
            if ($query instanceof Closure) {
                $callback = $query;
                $callback($query = $this->forSubQuery());
            }
            return $this->parseSub($query);
        });

        QueryBuilder::macro('parseSub', function ($query) {
            if ($query instanceof self || $query instanceof EloquentBuilder) {
                return [$query->toSql(), $query->getBindings()];
            } elseif (is_string($query)) {
                return [$query, []];
            } else {
                throw new InvalidArgumentException;
            }
        });
    }
}

