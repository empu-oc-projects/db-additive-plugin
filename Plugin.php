<?php

namespace Empu\DbAdditive;

use Backend\Facades\Backend;
use System\Classes\PluginBase;

/**
 * Database Additive Plugin Information File
 */
class Plugin extends PluginBase
{
    use Providers\QueryBuilderJoinSubTrait;

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'DB Additive',
            'description' => 'No description provided yet...',
            'author'      => 'Empu',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        $this->bootQueryBuilderJoinSubTrait();
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Empu\DatabaseAdditive\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'empu.databaseadditive.some_permission' => [
                'tab' => 'DatabaseAdditive',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'databaseadditive' => [
                'label'       => 'DatabaseAdditive',
                'url'         => Backend::url('empu/databaseadditive/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['empu.databaseadditive.*'],
                'order'       => 500,
            ],
        ];
    }
}
