<?php

namespace Empu\DbAdditive\Behaviors;

use October\Rain\Database\Builder;
use October\Rain\Database\Model;
use October\Rain\Extension\ExtensionBase;
use Ramsey\Uuid\Uuid;

class ExternalKey extends ExtensionBase
{
    /**
     * The external key for the model
     *
     * @var string
     * public $externalKey = 'ref';
     */

    /**
     * The model
     *
     * @var Model
     */
    protected $parentModel;

    /**
     * External key consturctor
     *
     * @param \October\Rain\Database\Model $model
     */
    public function __construct(Model $model)
    {
        $this->parentModel = $model;

        if (! $this->parentModel->propertyExists('externalKey')) {
            $this->parentModel->addDynamicProperty('externalKey', $this->parentModel->getKeyName());
        }
    }

    /**
     * Fill extenal key with generated unique string
     *
     * @param bool $forced
     * @return void
     */
    public function generateExternalKey($forced = false): void
    {
        if (empty($this->parentModel->getExternalKey()) || $forced) {
            $externalKey = $this->parentModel->getExternalKeyName();
            $externalKeyValue = $this->parentModel->externalKeyGenerator();

            $this->parentModel->{$externalKey} = $externalKeyValue;
        }
    }

    /**
     * Set external key for the model
     *
     * @param string $name
     * @return \October\Rain\Database\Model
     */
    public function setExternalKeyName(string $name): Model
    {
        $this->parentModel->externalKey = $name;

        return $this->parentModel;
    }

    /**
     * Get externa key for the model
     *
     * @return string
     */
    public function getExternalKeyName(): string
    {
        return $this->parentModel->externalKey;
    }

    /**
     * Get the value of model's external key
     *
     * @return string|null
     */
    public function getExternalKey(): ?string
    {
        return $this->parentModel->getAttribute($this->parentModel->getExternalKeyName());
    }

    /**
     * Generata unique string for model's external key value
     *
     * @return string
     */
    public function externalKeyGenerator(): string
    {
        return Uuid::uuid4()->toString();
    }

    /**
     * Scoping query by model's external key
     *
     * @param \October\Rain\Database\Builder $query
     * @param string $string
     * @return \October\Rain\Database\Builder
     */
    public function scopeByExternalKey(Builder $query, string $string): Builder
    {
        $externalKey = $this->getExternalKeyName();
        $column = $this->parentModel->qualifyColumn($externalKey);

        return $query->where($column, $string);
    }
}
